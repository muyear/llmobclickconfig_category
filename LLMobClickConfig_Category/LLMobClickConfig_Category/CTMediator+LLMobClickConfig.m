//
//  CTMediator+LLMobClickConfig.m
//  LLMobClickConfig_Category
//
//  Created by muyear on 2018/12/12.
//  Copyright © 2018 muyear. All rights reserved.
//

#import "CTMediator+LLMobClickConfig.h"
NSString * const kCTMediatorTargetLLMobClickConfig = @"LLMobClickConfig";
//初始化
NSString * const kCTMediatorActionStartWithAppKey = @"startWithAppKey";

//登录事件
NSString * const kCTMediatorActionEventLoginWithSource = @"eventLoginWithSource";
//获取手机验证码
NSString * const kCTMediatorActionEventLoginCaptha = @"eventLoginCaptha";
//登录结果
NSString * const kCTMediatorActionEventLoginResult = @"eventLoginResult";
//点击查看隐私
NSString * const kCTMediatorActionEventClickPrivacy = @"eventClickPrivacy";
//点击tag列表
NSString * const kCTMediatorActionEventClickTags = @"eventClickTags";

//浏览热卖会场
NSString * const kCTMediatorActionEventReviewHot = @"eventReviewHot";
//浏览预告会场
NSString * const kCTMediatorActionEventReviewPresale = @"eventReviewPresale";
//点击客服
NSString * const kCTMediatorActionEventClickedService = @"eventClickedService";

//点击会场列表
NSString * const kCTMediatorActionEventClickedActivityList = @"eventClickedActivityList";



//浏览商品详情
NSString * const kCTMediatorActionEventReviewProductDetail = @"eventReviewProductDetail";
//立即抢购
NSString * const kCTMediatorActionEventCventClickedBuy = @"eventClickedBuy";
//选择sku
NSString * const kCTMediatorActionEventCventClickedSku = @"eventClickedSku";
//付款
NSString * const kCTMediatorActionEventCventClickedPay = @"eventClickedPay";
//浏览秒杀订单
NSString * const kCTMediatorActionEventReviewOrder = @"eventReviewOrder";
//查看订单详情
NSString * const kCTMediatorActionEventReviewOrderDetail = @"eventReviewOrderDetail";
//浏览分类主页面
NSString * const kCTMediatorActionEventReviewCategory = @"eventReviewCategory";
//浏览我的页面
NSString * const kCTMediatorActionEventReviewMinePage = @"eventReviewMinePage";
//点击我的头衔
NSString * const kCTMediatorActionEventClickedHead = @"eventClickedHead";
//更换头像
NSString * const kCTMediatorActionEventClickedChangeHead= @"eventClickedChangeHead";
//点击昵称
NSString * const kCTMediatorActionEventClickedNickName = @"eventClickedNickName";



//点击收获地址
NSString * const kCTMediatorActionEventClickedAddress = @"eventClickedAddress";
//新增收获地址
NSString * const kCTMediatorActionEventClickedAddAddress= @"eventClickedAddAddress";
//删除收获地址
NSString * const kCTMediatorActionEventClickedDelAddress= @"eventClickedDelAddress";
//编辑收获地址
NSString * const kCTMediatorActionEventClickedEditeAddress = @"eventClickedEditeAddress";
//推送设置
NSString * const kCTMediatorActionEventClickedNotiSetting = @"eventClickedNotiSetting";
//点击浏览记录
NSString * const kCTMediatorActionEventClickedReviewHistory= @"eventClickedReviewHistory";
//点击清除缓存
NSString * const kCTMediatorActionEventClickedClearChache = @"eventClickedClearChache";
//点击退出登陆
NSString * const kCTMediatorActionEventClickedLogOut = @"eventClickedLogOut";
//点击切换语言
NSString * const kCTMediatorActionEventClickedSwitchLanguage = @"eventClickedSwitchLanguage";
//点击设置
NSString * const kCTMediatorActionEventClickedSetting = @"eventClickedSetting";
//点击搜索框
NSString * const kCTMediatorActionEventClickedSearchTF= @"eventClickedSearchTF";
//取消搜索
NSString * const kCTMediatorActionEventClickedCancleSearch = @"eventClickedCancelSearch";
//搜索商品
NSString * const kCTMediatorActionEventClickedSearchProduct = @"eventClickedSearchProduct";

//进入分类
NSString * const kCTMediatorActionEventClickedCategory = @"eventClickedCategory";
//进入分类详情
NSString * const kCTMediatorActionEventClickedCategoryDetail = @"eventClickedCategoryDetail";
//进入首页
NSString * const kCTMediatorActionEventClickedHome = @"eventClickedHome";
//点击首页banner
NSString * const kCTMediatorActionEventClickedHomeBanner = @"eventClickedHomeBanner";
//点击首页优品
NSString * const kCTMediatorActionEventClickedHomeYoupin = @"eventClickedHomeYoupin";
//点击频道
NSString * const kCTMediatorActionEventClickedChannel = @"eventClickedChannel";
//漏洞福利社
NSString * const kCTMediatorActionEventClickedWelfare = @"eventClickedWelfare";
//漏洞福利单
NSString * const kCTMediatorActionEventClickedWelfareDetail = @"eventClickedWelfareDetail";
//购物车
NSString * const kCTMediatorActionEventClickedShoppingCart = @"eventClickedShoppingCart";
//淘宝授权登录
NSString * const kCTMediatorActionEventClickedTBlogin = @"eventClickedTaoBaoLogin";
//点击淘宝商品搜索bar
NSString * const kCTMediatorActionEventClickedSearchBar = @"eventClickedSearchBar";
//淘宝商品搜索
NSString * const kCTMediatorActionEventClickedSearchTBProduct = @"eventClickedSearchTBProduct";
//淘宝商品详情
NSString * const kCTMediatorActionEventClickedTBProductDetail = @"eventClickedTBProductDetail";
//立即领券
NSString * const kCTMediatorActionEventClickedGetCoupon = @"eventClickedGetCoupon";
//复制淘口令
NSString * const kCTMediatorActionEventClickedCopyTKL = @"eventClickedCopyTKL";


//淘宝订单
NSString * const kCTMediatorActionEventClickedTaobaoOrder = @"eventClickedTaobaoOrder";
//关于好省
NSString * const kCTMediatorActionEventClickedAboutHaosheng = @"eventClickedAboutHaosheng";
//售后订单
NSString * const kCTMediatorActionEventClickedServerOrder = @"eventClickedServerOrder";
@implementation CTMediator (LLMobClickConfig)
- (void)LLMobClickConfig_startWithAppKey:(NSString *)appKey channelId:(NSString *)channelId bundleVersion:(NSString *)bundleVersion{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"appKey"] = appKey;
    params[@"channelId"] = channelId;
    params[@"bundleVersion"] = bundleVersion;
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionStartWithAppKey params:params shouldCacheTarget:NO];
}
//MARK:点击登录
- (void)LLMobClickConfig_eventLoginWithSource:(NSString *)from {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"from"] = from;
    params[@"event"] = @"hs_click_login";
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventLoginWithSource params:params shouldCacheTarget:NO];
    
    
}

//MARK:获取手机验证码
- (void)LLMobClickConfig_eventLoginCaptha:(NSString *)phone {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"phone"] = phone;
    params[@"event"] = @"hs_click_loginCaptha";
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventLoginCaptha params:params shouldCacheTarget:NO];
    
}

//MARK:登录结果
- (void)LLMobClickConfig_eventLoginReultWithSource:(NSString *)result {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"result"] = result;
    params[@"event"] = @"hs_login_result";
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventLoginResult params:params shouldCacheTarget:NO];
    
}

//MARK:点击查看隐私
- (void)LLMobClickConfig_eventClickedPrivacy {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_privacy";
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickPrivacy params:params shouldCacheTarget:NO];
    
}

//MARK:点击tag列表
- (void)LLMobClickConfig_eventClickedTags:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_tags";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickTags params:params shouldCacheTarget:NO];
    
}

//MARK:浏览热卖会场
- (void)LLMobClickConfig_eventReviewHot:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_review_hot";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventReviewHot params:params shouldCacheTarget:NO];
    
}

//MARK:浏览预告会场
- (void)LLMobClickConfig_eventReviewPresale:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_review_presale";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventReviewPresale params:params shouldCacheTarget:NO];
    
}

//MARK:点击客服
- (void)LLMobClickConfig_eventClickedService:(NSString *)userID frome:(NSString*)from{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_service";
    params[@"userID"] = userID;
    params[@"from"] = from;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedService params:params shouldCacheTarget:NO];
    
}

//MARK:点击会场列表
- (void)LLMobClickConfig_eventClickedActivityList:(NSString *)userID activityId:(NSString*)activityId activityName:(NSString*)activityName from:(NSString*)from{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_activityList_cick";
    params[@"userID"] = userID;
    params[@"activityId"] = activityId;
    params[@"activityName"] = activityName;
    params[@"from"] = from;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedActivityList params:params shouldCacheTarget:NO];
    
}

//MARK:浏览商品详情
- (void)LLMobClickConfig_eventReviewProductDetail:(NSString *)userID couponId:(NSString*)couponId coupontitle:(NSString*)coupontitle from:(NSString*)from{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_review_coupon_detail";
    params[@"userID"] = userID;
    params[@"coupontitle"] = coupontitle;
    params[@"couponId"] = couponId;
    params[@"from"] = from;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventReviewProductDetail params:params shouldCacheTarget:NO];
    
}

//MARK:立即抢购
- (void)LLMobClickConfig_eventClickedBuy:(NSString *)userID couponId:(NSString*)couponId {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_buy";
    params[@"userID"] = userID;
    params[@"couponId"] = couponId;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventCventClickedBuy params:params shouldCacheTarget:NO];
    
}

//MARK:选择sku
- (void)LLMobClickConfig_eventClickedSku:(NSString *)userID couponId:(NSString*)couponId  couponAttribute:(NSString*)couponAttribute{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_sku";
    params[@"userID"] = userID;
    params[@"couponId"] = couponId;
    params[@"couponAttribute"] = couponAttribute;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventCventClickedSku params:params shouldCacheTarget:NO];
    
}

//MARK:付款
- (void)LLMobClickConfig_eventClickedPay:(NSString *)userID couponId:(NSString*)couponId paychannel:(NSString*)paychannel{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_pay";
    params[@"userID"] = userID;
    params[@"couponId"] = couponId;
    params[@"paychannel"] = paychannel;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventCventClickedPay params:params shouldCacheTarget:NO];
    
}

//MARK:浏览秒杀订单
- (void)LLMobClickConfig_eventReviewOrder:(NSString *)userID group:(NSString*)group {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_review_order";
    params[@"userID"] = userID;
    params[@"group"] = group;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventReviewOrder params:params shouldCacheTarget:NO];
    
}

//MARK:订单详情
- (void)LLMobClickConfig_eventReviewOrderDetail:(NSString *)userID couponId:(NSString*)couponId {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_review_order_detail";
    params[@"userID"] = userID;
    params[@"couponId"] = couponId;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventReviewOrderDetail params:params shouldCacheTarget:NO];
    
}

//MARK:浏览分类主页面
- (void)LLMobClickConfig_eventReviewCategory:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_review_category_page";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventReviewCategory params:params shouldCacheTarget:NO];
    
}

//MARK:浏览我的页面
- (void)LLMobClickConfig_eventReviewMinePage:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_review_mine_page";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventReviewMinePage params:params shouldCacheTarget:NO];
    
}

//MARK:点击我的头衔
- (void)LLMobClickConfig_eventlickedHead:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_head";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedHead params:params shouldCacheTarget:NO];
    
}

//MARK:更换头像
- (void)LLMobClickConfig_eventClickedChangeHead:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_chang_head";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedChangeHead params:params shouldCacheTarget:NO];
    
}

//MARK:更换头像
- (void)LLMobClickConfig_eventClickedNickName:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_nick";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedNickName params:params shouldCacheTarget:NO];
    
}

//MARK:点击收获地址
- (void)LLMobClickConfig_eventClickedAddress:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_address";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedAddress params:params shouldCacheTarget:NO];
    
}

//MARK:新增收获地址
- (void)LLMobClickConfig_eventClickedAddAddress:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_add_address";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedAddAddress params:params shouldCacheTarget:NO];
    
}

//MARK:删除收获地址
- (void)LLMobClickConfig_eventClickedDelAddress:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_clcik_del_address";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedDelAddress params:params shouldCacheTarget:NO];
    
}

//MARK:编辑收获地址
- (void)LLMobClickConfig_eventClickedEditeAddress:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_edit_address";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedEditeAddress params:params shouldCacheTarget:NO];
    
}

//MARK:推送设置
- (void)LLMobClickConfig_eventClickedNotiSetting:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_push_setting";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedNotiSetting params:params shouldCacheTarget:NO];
    
}

//MARK:点击浏览记录
- (void)LLMobClickConfig_eventClickedReviewHistory:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_history";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedReviewHistory params:params shouldCacheTarget:NO];
    
}

//MARK:点击清除缓存
- (void)LLMobClickConfig_eventClickedClearChache:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_clear_cache";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedClearChache params:params shouldCacheTarget:NO];
    
}

//MARK:点击退出登陆
- (void)LLMobClickConfig_eventClickedLogOut:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_exit_app";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedLogOut params:params shouldCacheTarget:NO];
}

//MARK:点击切换语言
- (void)LLMobClickConfig_eventClickedSwitchLanguage:(NSString *)userID language:(NSString *)language{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_change_language";
    params[@"userID"] = userID;
    params[@"language"] = language;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedSwitchLanguage params:params shouldCacheTarget:NO];
}

//MARK:点击设置
- (void)LLMobClickConfig_eventClickedSSetting:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_mine_setting";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedSetting params:params shouldCacheTarget:NO];
}

//MARK:点击搜索框
- (void)LLMobClickConfig_eventClickedSearchTF:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_begin_search";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedSearchTF params:params shouldCacheTarget:NO];
}

//MARK:取消搜索
- (void)LLMobClickConfig_eventClickedCancelSearch:(NSString *)userID {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_cancel_search";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedCancleSearch params:params shouldCacheTarget:NO];
}

//MARK:搜索商品
- (void)LLMobClickConfig_eventClickedSearchProduct:(NSString *)userID searchKey:(NSString *)searchKey{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_search_coupon";
    params[@"userID"] = userID;
    params[@"searchKey"] = searchKey;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedSearchProduct params:params shouldCacheTarget:NO];
}

//MARK:进入分类
- (void)LLMobClickConfig_eventClickedCategory:(NSString *)userID{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_product_category";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedCategory params:params shouldCacheTarget:NO];
}
//MARK:进入分类详情
- (void)LLMobClickConfig_eventClickedCategoryDetail:(NSString *)userID itemId:(nonnull NSString *)itemId{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_category_detail";
    params[@"userID"] = userID;
    params[@"itemId"] = itemId;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedCategoryDetail params:params shouldCacheTarget:NO];
}
//MARK:进入首页
- (void)LLMobClickConfig_eventClickedHome:(NSString *)userID{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_home";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedHome params:params shouldCacheTarget:NO];
}
//MARK:点击首页banner
- (void)LLMobClickConfig_eventClickedHomeBanner:(NSString *)userID itemId:(NSString *)itemId{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_home_banner";
    params[@"userID"] = userID;
    params[@"itemId"] = itemId;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedHomeBanner params:params shouldCacheTarget:NO];
}
//MARK:点击首页优品
- (void)LLMobClickConfig_eventClickedHomeYoupin:(NSString *)userID itemId:(NSString *)itemId{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_home_youpin";
    params[@"userID"] = userID;
    params[@"itemId"] = itemId;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedHomeYoupin params:params shouldCacheTarget:NO];
}
//MARK:点击频道
- (void)LLMobClickConfig_eventClickedChannel:(NSString *)userID itemId:(NSString *)itemId{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_home_channel";
    params[@"userID"] = userID;
    params[@"itemId"] = itemId;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedChannel params:params shouldCacheTarget:NO];
}
//MARK:漏洞福利社
- (void)LLMobClickConfig_eventEventClickedWelfare:(NSString *)userID{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_welfare";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedWelfare params:params shouldCacheTarget:NO];
}
//MARK:漏洞福利单
- (void)LLMobClickConfig_eventClickedWelfareDetail:(NSString *)userID itemId:(NSString *)itemId{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_welfare_detail";
    params[@"userID"] = userID;
    params[@"itemId"] = itemId;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedWelfareDetail params:params shouldCacheTarget:NO];
}
//MARK:购物车
- (void)LLMobClickConfig_eventClickedShoppingCart:(NSString *)userID{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_shopping_cart";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedShoppingCart params:params shouldCacheTarget:NO];
}
//MARK:淘宝授权登录
- (void)LLMobClickConfig_eventClickedTBlogin:(NSString *)userID{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_tb_login";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedTBlogin params:params shouldCacheTarget:NO];
}
//MARK:点击淘宝商品搜索bar
- (void)LLMobClickConfig_eventClickedSearchBar:(NSString *)userID from:(NSString *)from{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_search_tb";
    params[@"userID"] = userID;
    params[@"from"] = from;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedSearchBar params:params shouldCacheTarget:NO];
}
//MARK:淘宝商品搜索
- (void)LLMobClickConfig_eventClickedSearchTBProduct:(NSString *)userID keyword:(NSString *)keyword type:(NSString *)type{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_search_tb_detail";
    params[@"userID"] = userID;
    params[@"keyword"] = keyword;
    params[@"type"] = type;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedSearchTBProduct params:params shouldCacheTarget:NO];
}
//MARK:淘宝商品详情
- (void)LLMobClickConfig_eventClickedTBProductDetail:(NSString *)userID itemId:(NSString *)itemId{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_tb_product_detail";
    params[@"userID"] = userID;
    params[@"itemId"] = itemId;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedTBProductDetail params:params shouldCacheTarget:NO];
}
//MARK:立即领券
- (void)LLMobClickConfig_eventClickedGetCoupon:(NSString *)userID itemId:(NSString *)itemId{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_get_coupon";
    params[@"userID"] = userID;
    params[@"itemId"] = itemId;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedGetCoupon params:params shouldCacheTarget:NO];
}
//MARK:复制淘口令
- (void)LLMobClickConfig_eventClickedCopyTKL:(NSString *)userID itemId:(NSString *)itemId{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_copy_tkl";
    params[@"userID"] = userID;
    params[@"itemId"] = itemId;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedCopyTKL params:params shouldCacheTarget:NO];
}
//MARK:淘宝订单
- (void)LLMobClickConfig_eventClickedTaobaoOrder:(NSString *)userID{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_taobaoOrder";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedTaobaoOrder params:params shouldCacheTarget:NO];
}
//MARK:关于好省
- (void)LLMobClickConfig_eventClickedAboutHaosheng:(NSString *)userID{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_about";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedAboutHaosheng params:params shouldCacheTarget:NO];
}
//MARK:售后订单
- (void)LLMobClickConfig_eventClickedServerOrder:(NSString *)userID{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"event"] = @"hs_click_serverOrder";
    params[@"userID"] = userID;
    params[@"opTime"] = [[self class] getCurrentTimes];
    [self performTarget:kCTMediatorTargetLLMobClickConfig action:kCTMediatorActionEventClickedServerOrder params:params shouldCacheTarget:NO];
}
+ (NSString*)getCurrentTimes{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *datenow = [NSDate date];
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    return currentTimeString;
    
}
@end

