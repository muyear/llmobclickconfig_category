//
//  CTMediator+LLMobClickConfig.h
//  LLMobClickConfig_Category
//
//  Created by muyear on 2018/12/12.
//  Copyright © 2018 muyear. All rights reserved.
//

#import <CTMediator/CTMediator.h>

NS_ASSUME_NONNULL_BEGIN

@interface CTMediator (LLMobClickConfig)
- (void)LLMobClickConfig_startWithAppKey:(NSString *)appKey channelId:(NSString *)channelId bundleVersion:(NSString *)bundleVersion;
//MARK:点击登录
- (void)LLMobClickConfig_eventLoginWithSource:(NSString *)from;

//MARK:获取手机验证码
- (void)LLMobClickConfig_eventLoginCaptha:(NSString *)phone;

//MARK:登录结果
- (void)LLMobClickConfig_eventLoginReultWithSource:(NSString *)result;

//MARK:点击查看隐私
- (void)LLMobClickConfig_eventClickedPrivacy;

//MARK:点击tag列表
- (void)LLMobClickConfig_eventClickedTags:(NSString *)userID;

//MARK:浏览热卖会场
- (void)LLMobClickConfig_eventReviewHot:(NSString *)userID;

//MARK:浏览预告会场
- (void)LLMobClickConfig_eventReviewPresale:(NSString *)userID;

//MARK:点击客服
- (void)LLMobClickConfig_eventClickedService:(NSString *)userID frome:(NSString*)from;

//MARK:点击会场列表
- (void)LLMobClickConfig_eventClickedActivityList:(NSString *)userID activityId:(NSString*)activityId activityName:(NSString*)activityName from:(NSString*)from;
//MARK:浏览商品详情
- (void)LLMobClickConfig_eventReviewProductDetail:(NSString *)userID couponId:(NSString*)couponId coupontitle:(NSString*)coupontitle from:(NSString*)from;

//MARK:立即抢购
- (void)LLMobClickConfig_eventClickedBuy:(NSString *)userID couponId:(NSString*)couponId;

//MARK:选择sku
- (void)LLMobClickConfig_eventClickedSku:(NSString *)userID couponId:(NSString*)couponId  couponAttribute:(NSString*)couponAttribute;

//MARK:付款
- (void)LLMobClickConfig_eventClickedPay:(NSString *)userID couponId:(NSString*)couponId paychannel:(NSString*)paychannel;

//MARK:浏览秒杀订单
- (void)LLMobClickConfig_eventReviewOrder:(NSString *)userID group:(NSString*)group;

//MARK:订单详情
- (void)LLMobClickConfig_eventReviewOrderDetail:(NSString *)userID couponId:(NSString*)couponId;

//MARK:浏览分类主页面
- (void)LLMobClickConfig_eventReviewCategory:(NSString *)userID;

//MARK:浏览我的页面
- (void)LLMobClickConfig_eventReviewMinePage:(NSString *)userID;

//MARK:点击我的头衔
- (void)LLMobClickConfig_eventlickedHead:(NSString *)userID;

//MARK:更换头像
- (void)LLMobClickConfig_eventClickedChangeHead:(NSString *)userID;

//MARK:更换头像
- (void)LLMobClickConfig_eventClickedNickName:(NSString *)userID;

//MARK:点击收获地址
- (void)LLMobClickConfig_eventClickedAddress:(NSString *)userID;

//MARK:新增收获地址
- (void)LLMobClickConfig_eventClickedAddAddress:(NSString *)userID;

//MARK:删除收获地址
- (void)LLMobClickConfig_eventClickedDelAddress:(NSString *)userID;

//MARK:编辑收获地址
- (void)LLMobClickConfig_eventClickedEditeAddress:(NSString *)userID;

//MARK:推送设置
- (void)LLMobClickConfig_eventClickedNotiSetting:(NSString *)userID;

//MARK:点击浏览记录
- (void)LLMobClickConfig_eventClickedReviewHistory:(NSString *)userID;

//MARK:点击清除缓存
- (void)LLMobClickConfig_eventClickedClearChache:(NSString *)userID;

//MARK:点击退出登陆
- (void)LLMobClickConfig_eventClickedLogOut:(NSString *)userID;

//MARK:点击切换语言
- (void)LLMobClickConfig_eventClickedSwitchLanguage:(NSString *)userID language:(NSString *)language;

//MARK:点击设置
- (void)LLMobClickConfig_eventClickedSSetting:(NSString *)userID;

//MARK:点击搜索框
- (void)LLMobClickConfig_eventClickedSearchTF:(NSString *)userID;

//MARK:取消搜索
- (void)LLMobClickConfig_eventClickedCancelSearch:(NSString *)userID;

//MARK:搜索商品
- (void)LLMobClickConfig_eventClickedSearchProduct:(NSString *)userID searchKey:(NSString *)searchKey;
//MARK:进入分类
- (void)LLMobClickConfig_eventClickedCategory:(NSString *)userID;
//MARK:进入分类详情
- (void)LLMobClickConfig_eventClickedCategoryDetail:(NSString *)userID itemId:(NSString *)itemId;
//MARK:进入首页
- (void)LLMobClickConfig_eventClickedHome:(NSString *)userID;
//MARK:点击首页banner
- (void)LLMobClickConfig_eventClickedHomeBanner:(NSString *)userID itemId:(NSString *)itemId;
//MARK:点击首页优品
- (void)LLMobClickConfig_eventClickedHomeYoupin:(NSString *)userID itemId:(NSString *)itemId;
//MARK:点击频道
- (void)LLMobClickConfig_eventClickedChannel:(NSString *)userID itemId:(NSString *)itemId;
//MARK:漏洞福利社
- (void)LLMobClickConfig_eventEventClickedWelfare:(NSString *)userID;
//MARK:漏洞福利单
- (void)LLMobClickConfig_eventClickedWelfareDetail:(NSString *)userID itemId:(NSString *)itemId;
//MARK:购物车
- (void)LLMobClickConfig_eventClickedShoppingCart:(NSString *)userID;
//MARK:淘宝授权登录
- (void)LLMobClickConfig_eventClickedTBlogin:(NSString *)userID;
//MARK:点击淘宝商品搜索bar
- (void)LLMobClickConfig_eventClickedSearchBar:(NSString *)userID from:(NSString *)from;
//MARK:淘宝商品搜索
- (void)LLMobClickConfig_eventClickedSearchTBProduct:(NSString *)userID keyword:(NSString *)keyword type:(NSString *)type;
//MARK:淘宝商品详情
- (void)LLMobClickConfig_eventClickedTBProductDetail:(NSString *)userID itemId:(NSString *)itemId;
//MARK:立即领券
- (void)LLMobClickConfig_eventClickedGetCoupon:(NSString *)userID itemId:(NSString *)itemId;
//MARK:复制淘口令
- (void)LLMobClickConfig_eventClickedCopyTKL:(NSString *)userID itemId:(NSString *)itemId;
//MARK:淘宝订单
- (void)LLMobClickConfig_eventClickedTaobaoOrder:(NSString *)userID;
//MARK:关于好省
- (void)LLMobClickConfig_eventClickedAboutHaosheng:(NSString *)userID;
//MARK:售后订单
- (void)LLMobClickConfig_eventClickedServerOrder:(NSString *)userID;
@end

NS_ASSUME_NONNULL_END

